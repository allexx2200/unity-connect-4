﻿using UnityEngine;
using System.Collections;

public class splash_game : MonoBehaviour {
	public Camera cam;

	private const int SHOW_OFF_RISE = 1;
	private const int SHOW_OFF_FALL = 2;
	private const int WAIT = 3;
	private const int ENTER_EASY = 4;
	private const int ENTER_HARD = 5;
	private const int ENTER_EXIT = 6;
	private const int FINAL_WAIT = 7;

	private float wait = 1f;
	private int current_state;

	public GameObject easy_button, hard_button, exit_button;

	// Use this for initialization
	void Start () {
		Cursor.visible = true;
		this.transform.localScale = new Vector2 (1, 1);
		current_state = SHOW_OFF_RISE;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 current_position;

		switch(current_state){
			case SHOW_OFF_RISE:
				Vector2 current_size = this.transform.localScale;
				Debug.Log(this.transform.localScale.x + "," + this.transform.localScale.y);
				Debug.Log("Current state: " + current_state);
				if(current_size.x < 1.20f){
					this.transform.localScale = current_size + new Vector2(0.005f, 0.005f);	
				} else {
					current_state = WAIT;
				}
				break;
			case SHOW_OFF_FALL:
				Vector2 current_size2 = this.transform.localScale;

				current_position = this.transform.position;
				if(current_size2.x > 1f || current_position.y < 12){
					if(current_size2.x > 1f) this.transform.localScale = current_size2 - new Vector2(0.005f, 0.005f);	
					if(current_position.y < 12) this.transform.position = current_position + new Vector3(0, 10f, 0) * Time.deltaTime;
				} else {
					current_state = ENTER_EASY;
				}
				break;
			case WAIT:
				wait -= Time.deltaTime;
				if(wait <= 0){
					current_state = SHOW_OFF_FALL;
					//Application.LoadLevel("test");
				}
				break;

			case ENTER_EASY:
				current_position = easy_button.transform.position;
				if(current_position.x < 0f){
				easy_button.transform.position = current_position + Time.deltaTime * new Vector3(100f,0f,0f);
				} else {
					easy_button.transform.position = new Vector3(0f, 5f, 0f);
					current_state = ENTER_HARD;
				}
				break;

			case ENTER_HARD:
				current_position = hard_button.transform.position;
				if(current_position.x < 0f){
					hard_button.transform.position = current_position + Time.deltaTime * new Vector3(100f,0f,0f);
				} else {
					hard_button.transform.position = new Vector3(0f, 0f, 0f);
					current_state = ENTER_EXIT;
				}
				break;

			case ENTER_EXIT:
				current_position = exit_button.transform.position;
				if(current_position.x < 0f){
					exit_button.transform.position = current_position + Time.deltaTime * new Vector3(100f,0f,0f);
				} else {
					exit_button.transform.position = new Vector3(0f, -5f, 0f);
					current_state = ENTER_EXIT;
				}
				break;
			default:
				break;
		}
	}

	void move(Vector3 destination, float speed){
		Vector3 current_position = this.transform.position;
		Vector3 next_position = current_position + speed/((destination - current_position).magnitude)*(destination - current_position);
	}
}
