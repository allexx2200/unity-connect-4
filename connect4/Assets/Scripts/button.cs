﻿using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {
	public enum button_type{
		FACEBOOK_BUTTON,
		TWITTER_BUTTON,
		EASY_GAME,
		HARD_GAME,
		EXIT
	};
	public button_type type;
	public Sprite passive;
	public Sprite hover;

	private SpriteRenderer spriteRenderer; 
	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
		if (spriteRenderer.sprite == null) // if the sprite on spriteRenderer is null then
			spriteRenderer.sprite = passive; // set the sprite to sprite1
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("escape")) {
			exit_behaviour();
		}
	}

	// The mesh goes red when the mouse is over it...
	void OnMouseEnter () {
		spriteRenderer.sprite = hover;
	}

	// ...and the mesh finally turns white when the mouse moves away.
	void OnMouseExit () {
		spriteRenderer.sprite = passive;
	}

	void OnMouseDown(){
		switch (type) {
			case button_type.FACEBOOK_BUTTON:
				facebook_behaviour();
				break;

			case button_type.TWITTER_BUTTON:
				twitter_behaviour();
				break;

			case button_type.EASY_GAME:
				easy_game();
				break;

			case button_type.HARD_GAME:
				hard_game();
				break;

			case button_type.EXIT:
				exit_behaviour();
				break;
		}
	} 

	void facebook_behaviour(){
		Application.OpenURL ("https://www.facebook.com/corporatebulldogstudios");
	}

	void twitter_behaviour(){
		Application.OpenURL ("https://twitter.com/corp_bulldog");
	}

	void easy_game(){
		Application.LoadLevel("easy");
	}

	void hard_game(){
		Application.LoadLevel("hard");
	}

	void exit_behaviour(){
		Application.Quit();
	}
}
