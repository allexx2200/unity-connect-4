﻿using UnityEngine;
using System.Collections;

public class game_master : MonoBehaviour {
	const int NO_DISC     = 0;
	const int RED_DISC    = 1;
	const int YELLOW_DISC = 2;
	
	public const float PLACEMENT_HEIGHT = 3.5f;

	public float[] bredth = new float[] {-3f, -2f, -1f, 0f, 1f, 2f, 3f};
	public float[] depth  = new float[] {2.5f, 1.5f, 0.5f, -0.5f, -1.5f, -2.5f};

	int[,] board = 	new int[6,7];

	bool[] is_dropable = new bool[7];
	bool players_turn;
	
	int humans_disc, ai_disc;

	public GameObject red_disc, yellow_disc;

	/* AI-ul */
	CF_BOT bot;
	bool endedGame = false;

	public enum dificulty {
		EASY,
		HARD
	}

	public dificulty dif;
	private float end_time;

	// Use this for initialization
	void Start () {
		end_time = 3f;

		for (int i=0; i < 6; i++) {
			for(int j=0; j < 7; j++) {
				board[i,j] = NO_DISC;
			}
		}
		for (int i=0; i < 7; i++) {
			is_dropable[i] = true;
		}

		players_turn = true;
		humans_disc = RED_DISC;
		ai_disc = YELLOW_DISC;

		Cursor.visible = false;

		if (dif == dificulty.EASY) {
			bot = new RandomBot ();
		} else {
			bot = new RandomBot ();
		}
	}

	Object current_disc = null;

	// Update is called once per frame
	void Update () {
		Cursor.visible = false;
		if (Input.GetKey ("escape")) {
			Application.Quit();
		}
		if (endedGame) {
			end_game();
			return;
		}
		if (players_turn) {
			//instantiaza un nou disc de culoarea humans_disc
			int current_column = 0;
			Vector3 place = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if(place.x < bredth[0]){
				place.x = bredth[0];
			} else if(place.x > bredth[6]){
				place.x = bredth[6];
				current_column = 6;
			} else {
				for(int i=0; i < 7 ; i++){
					if(place.x > bredth[i] && place.x < bredth[i+1]){
						place.x = bredth[i];
						current_column = i;
						break;
					}
				}
			}
			place.y = PLACEMENT_HEIGHT;
			place.z = 0f;
			if(current_disc == null){
				current_disc = Instantiate((humans_disc == YELLOW_DISC)?yellow_disc:red_disc, place,Quaternion.identity);
			} else {
				((GameObject)current_disc).transform.position = place;
			}

			if(Input.GetMouseButtonDown(0) && board[0,current_column] == NO_DISC){

				//make move
				int where = 0;
				for(int i = 5; i >= 0; i--){
					if(board[i,current_column] == NO_DISC){
						where = i;
						break;
					}
				}

				board[where,current_column] = humans_disc;
				((GameObject)current_disc).transform.position = new Vector3(bredth[current_column],depth[where],0f);

				players_turn = false;
				current_disc = null;

				if(hasEnded()){
					endedGame = true;
					return;
				}

				makeMove();

				endedGame = hasEnded();
			}
		}
	}

	void end_game(){
		if (end_time > 0) {
			end_time -= Time.deltaTime;
		} else {
			Application.LoadLevel("splash_game");
		}
	}

	void makeMove(){
		do {
			move m = bot.doMove (board);
			if(!isValid(m)){
				continue;
			}

			board[m.x,m.y] = ai_disc;
			Object oponent_disc = Instantiate((humans_disc == RED_DISC)?yellow_disc:red_disc,new Vector3(bredth[m.y],depth[m.x],0f),Quaternion.identity);

			players_turn = true;
			return;

		} while(true);

	}

	//check if the move is valid
	bool isValid(move m){
		try{
			return board [m.x, m.y] == NO_DISC;
		}catch(System.IndexOutOfRangeException  e){
			return false;
		}
	}

	bool hasEnded(){
		for (int i=0; i < 6; i++) {
			for(int j=0; j < 7; j++){
				try{
					if(board[i,j] == board[i+1,j] && 
					   board[i+1,j] == board[i+2,j] && 
					   board[i+2,j] == board[i+3,j] && 
					   board[i,j] != NO_DISC){
						return true;
					}
				}catch(System.IndexOutOfRangeException e){}

				try{
				if(board[i,j] == board[i,j+1] && 
					   board[i,j+1] == board[i,j+2] && 
					   board[i,j+2] == board[i,j+3] && 
					   board[i,j] != NO_DISC){
						return true;
					}
				}catch(System.IndexOutOfRangeException e){}
				try{
				if(board[i,j] == board[i+1,j+1] && 
					   board[i+1,j+1] == board[i+2,j+2] && 
					   board[i+2,j+2] == board[i+3,j+3] && 
					   board[i,j] != NO_DISC){
						return true;
					}
				}catch(System.IndexOutOfRangeException e){}
				try{
					if(board[i  ,j+3] == board[i+1,j+2] && 
					   board[i+1,j+2] == board[i+2,j+1] && 
					   board[i+2,j+1] == board[i+3,j] && 
					   board[i,j] != NO_DISC){
						return true;
					}
				}catch(System.IndexOutOfRangeException e){}
			}
		}
		return false;
	}

	//MOVE CLASS
	class move{ 
		public int x,y; 
		public move(int a, int b){
			x = a;
			y = b;
		}
	}

	//BOT INTERFACE
	interface CF_BOT{
		move doMove(int[,] board);
	}

	//RANDOM BOT
	class RandomBot : CF_BOT{
		public move doMove(int[,] board){
			do {
				int x = (int)Mathf.Floor(Random.Range(0,7));
				if(board[0,x] != NO_DISC){
					continue;
				}

				for(int i=5; i >= 0; i--){
					if(board[i,x] == NO_DISC){
						return new move(i,x);
					}
				}
			} while(true);
		}
	}
}
