﻿using UnityEngine;
using System.Collections;

public class splash : MonoBehaviour {
	public Camera cam;

	private float wait = 5f;

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		this.transform.localScale = new Vector2 (5, 5);
	}
	
	// Update is called once per frame
	void Update () {
		float sc = this.transform.localScale.x * 0.97f;
		if (sc > 1) {
			this.transform.localScale = new Vector2 (sc, sc);

		} else {
			this.transform.localScale = new Vector2 (1, 1);
			wait -= Time.deltaTime;
			if(wait <= 0){
				Application.LoadLevel("splash_game");
			}
		}
	}
}
